#include <string>
#include <iostream>
#include <iterator>
#include <vector>
#include <functional>
#include <algorithm>
#include <cmath>

using namespace std;

template<typename T, class Fct>
void map(vector<T> const& v, Fct p) {
  for (auto const& x: v) { p(x); }
}

int
main(int argv, char* argc[]) {
  vector<int>   vi = { 1, 3, 41, 1968, 1971, 2018 };
  map(vi,
      [](int const& x) {
	cout << "sqrt(" << x << ")->" << sqrt(x) << '\n';
      });
  vector<float> vf(vi.size());
  std::transform(vi.begin(), vi.end(), vf.begin(),
		 [](int i) { return sqrt(i); });
  std::copy(vf.begin(), vf.end(), ostream_iterator<float>(cout, " "));
  return 0;
}
