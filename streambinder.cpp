#include <string>
#include <iostream>
#include <fstream>
#include <vector>
#include <cmath>

using namespace std;

class printer {
public:
  printer(ostream& out) : my_output(out) {}
  void operator()(int i) { my_output << i; }
private:
  ostream& my_output;
};

void
mapvi(vector<int> const& v, printer& p) {
  for (int x: v) {  p(x); }
}

int
main(int argc, char* argv[]) {
  vector<int>    vi = { 1, 3, 41, 1968, 1971, 2018 };
  printer printout(cout);
  for (auto x: vi) {  printout(x); }  cout << '\n';
  printer printerr(cerr);
  for (auto x: vi) {  printerr(x);  }  cerr << '\n';
  mapvi(vi, printout); cout << '\n';
  if (argc == 2) {
    ofstream file(argv[1]);
    printer printf(file);
    mapvi(vi, printf); file << '\n';
  }
  return 0;
}
