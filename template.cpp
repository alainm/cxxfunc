#include <string>
#include <iostream>
#include <vector>
#include <cmath>

using namespace std;

template<typename T>
void print(T x)  {  cout << x; }

int
main(int argv, char* argc[]) {
  vector<int>    vi = { 1, 3, 41, 1968, 1971, 2018 };
  vector<float>  vf = { 1, 3, 41.9, 1968, 1971, 2018 };
  vector<string> vs = { "un", "trois", "1971", "znort" };
  
  for (auto x: vi) {  print(x); }  cout << '\n';
  for (auto x: vf) {  print(x); }  cout << '\n';
  for (auto x: vs) {  print(x); }  cout << '\n';
  return 0;
}
