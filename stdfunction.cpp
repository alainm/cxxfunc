#include <string>
#include <iostream>
#include <vector>
#include <functional>

using namespace std;

template<typename T>
class printer {
public:
  printer(ostream& out, string sep = " ") : my_output(out), my_sep(sep) {}
  void operator()(T const& x) { my_output << x << my_sep; }
private:
  ostream& my_output;
  string   my_sep;
};

template<typename T>
void map(vector<T>& v, function<void (T const&)> p) {
  for (auto const& x: v) { p(x); }
}

void printc(char const& c) { cout << c << ' '; }

int
main(int argv, char* argc[]) {
  vector<int>   vi = { 1, 3, 41, 1968, 1971, 2018 };
  vector<char>  vc = { 'a', 'A', 96 };
  std::function<void (int const&)> pi = printer<int>(cout, "\t");
  map(vi, pi); cout << '\n';
  std::function<void (char const&)> pc = printer<char>(cout, "\t");
  map(vc, pc); cout << '\n';
  pc = printc;
  map(vc, pc); cout << '\n';
  return 0;
}
