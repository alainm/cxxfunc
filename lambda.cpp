#include <string>
#include <iostream>
#include <vector>
#include <functional>

using namespace std;

template<typename T, class Fct>
void map(vector<T> const& v, Fct p) {
  for (auto const& x: v) { p(x); }
}

int
main(int argv, char* argc[]) {
  vector<int>   vi = { 1, 3, 41, 1968, 1971, 2018 };
  vector<char>  vc = { 'a', 'A', 96 };
  map(vi,[](int x) {cout << x << ' ';});
  cout << '\n';
  map(vc,[](char x) {cout << x << ' ';});
  cout << '\n';
  return 0;
}
