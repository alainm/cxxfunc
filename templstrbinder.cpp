#include <string>
#include <iostream>
#include <vector>
#include <cmath>

using namespace std;

template<typename T>
class printer {
public:
  printer(ostream& out, string sep = " ") : my_output(out), my_sep(sep) {}
  void operator()(T const& x) { my_output << x << my_sep; }
private:
  ostream& my_output;
  string   my_sep;
};

template<typename T>
void print(vector<T> const& v, printer<T>& p) {
  for (auto const& x: v) { p(x); }
}

int
main(int argv, char* argc[]) {
  vector<int>   vi = { 1, 3, 41, 1968, 1971, 2018 };
  vector<char>  vc = { 'a', 'A', 96 };
  printer<int> pi(cout, "\t");
  printer<char> pc(cout);
  print(vi, pi); cout << '\n';
  print(vc, pc); cout << '\n';
  return 0;
}
