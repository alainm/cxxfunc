#include <functional>
#include <string>
#include <vector>
#include <map>
#include <iostream>
#include <algorithm>
#include <iterator>

using namespace std;

class stuff {
public:
  typedef std::function<void (stuff&)> callback;
  
  stuff(string name, float value = 0)
    : my_name(name), my_value(value) {}
  stuff(stuff const& s)  = default;
  stuff(stuff&& s) = default;
  ~stuff() = default;
  std::string name()  const { return my_name; }
  float       value() const { return my_value; }

  float add_value(float d) { my_value += d; }
  float remove_value(float d) { add_value(-d); }

  void register_callback(std::string name, callback cb) {
    my_callbacks[name] = cb;
  }
  void trigger_callback(std::string name) {
    callback cb = my_callbacks[name];
    cb(*this);
  }
  
private:
  string my_name;
  float my_value;
  std::map<string, callback> my_callbacks;
};

ostream& operator<<(ostream& out, stuff const& s) {
  out << '{' << s.name() << ':' << s.value() << '}';
  return out;
}

struct bubble {
  bubble(float r) : ratio(r) {}
  void operator()(stuff& s) { s.add_value(s.value()*ratio); }
  float ratio;
};

void life(vector<stuff>& stuffs);

int
main(int argc, char * argv[]) {
  vector<stuff> stuffs;
  stuffs.push_back(stuff("cheap stuff", 10));
  stuffs.push_back(stuff("expensive stuff", 2000));
  std::copy(stuffs.begin(), stuffs.end(), ostream_iterator<stuff>(cout, " "));
  cout << '\n';
  stuffs[0].register_callback("DEVALUATE",
			      [](stuff& s) { s.remove_value(s.value()*0.9); });
  stuffs[1].register_callback("DEVALUATE",
			      [](stuff& s) { s.remove_value(s.value()*0.12); });
  stuffs[0].register_callback("TAX",
			      [](stuff& s) { s.remove_value(s.value()*0.19); });
  stuffs[1].register_callback("TAX",
			      [](stuff& s) { s.remove_value(s.value()*0.33); });
  stuffs[1].register_callback("BUBBLE", bubble(3.4));
  stuffs[0].register_callback("BUBBLE", bubble(1.1));

  life(stuffs);
  std::copy(stuffs.begin(), stuffs.end(), ostream_iterator<stuff>(cout, " "));
  cout << '\n';
  
  return 0;
}

void
life(vector<stuff>& stuffs) {
  for(stuff& s : stuffs) {
    s.trigger_callback("TAX");
    s.trigger_callback("DEVALUATE");
    s.trigger_callback("BUBBLE");    
  }
}
