#include <string>
#include <iostream>
#include <fstream>
#include <vector>
#include <cmath>

using namespace std;

class printer {
public:
  printer(ostream& out) : my_output(out) {}
  void operator()(int i) { my_output << i; }
private:
  ostream& my_output;
};

template<typename T> void print(T const& x) { cout << x << ' ';}

template<class Cont, class Printer>
void
print(Cont const& v, Printer& p) {
  for (auto x: v) {  p(x); }
}

int
main(int argc, char* argv[]) {
  vector<int>    vi = { 1, 3, 41, 1968, 1971, 2018 };
  printer printout(cout);
  print(vi, printout); cout << '\n';
  print(vi, print<int>); cout << '\n';
  print(vi, print<float>); cout << '\n';
  std::vector<char const*> cstr = { "bob", "alice"};
  print( cstr, printf); cout << '\n';
  return 0;
}
