#include <string>
#include <iostream>
#include <vector>
#include <cmath>

using namespace std;

void print(int   i)  {  cout << i; }
void print(float f)  {  cout << f; }
void print(string s) {  cout << s; }

int
main(int argv, char* argc[]) {
  vector<int>    vi = { 1, 3, 41, 1968, 1971, 2018 };
  vector<float>  vf = { 1, 3, 41, 1968, 1971, 2018 };
  vector<string> vs = { "1", "3", "41", "1968", "1971", "2018" };
  
  for (auto x: vi) {  print(x); }  cout << '\n';
  for (auto x: vf) {  print(x); }  cout << '\n';
  for (auto x: vs) {  print(x); }  cout << '\n';
  return 0;
}
