#include <string>
#include <iostream>
#include <vector>
#include <cmath>

using namespace std;

class ugly_printer {
public:
  void print(int i) const { cout << i; }
};

class printer {
public:
  void operator()(int i) const { cout << i; }
};

int
main(int argv, char* argc[]) {
  vector<int>    vi = { 1, 3, 41, 1968, 1971, 2018 };
  ugly_printer duck;
  for (auto x: vi) {  duck.print(x); }  cout << '\n';
  printer swan, print;
  for (auto x: vi) {  swan(x);  }  cout << '\n';
  for (auto x: vi) {  print(x); }  cout << '\n';
return 0;
}
