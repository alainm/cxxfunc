#include <iostream>
#include <vector>

using namespace std;

void print(int i) {  cout << i; }

int
main(int argv, char* argc[]) {
  // single value:
  print(12);
  cout << '\n';
  vector<int> v = { 1, 3, 41, 1968, 1971, 2018 };
  // on a vector
  for (int i: v) {  print(i); }
  cout << '\n';
}
